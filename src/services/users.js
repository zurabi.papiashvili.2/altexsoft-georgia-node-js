const knex = require('../knex');
const fs = require('fs');
const USER_TABLE = 'users';

async function uploadImage(avatar) {
  const uniqueName = `./public/avatars/${Date.now()}-${Math.round(Math.random() * 1e9)}.png`;
  const base64Data = avatar.replace(/^data:image\/\w+;base64,/, '');
  fs.writeFile(uniqueName, base64Data, { encoding: 'base64' }, (err) => {
    if (err) {
      return 'error';
    }
    return 'success';
  });
  return uniqueName;
}
module.exports = {
  async getById(id) {
    const item = await knex(USER_TABLE).select('*').where({ id }).first();
    return item;
  },
  async getList() {
    const item = await knex(USER_TABLE).select('*');
    return item;
  },

  async addItem(item) {
    item.avatar = await uploadImage(item.avatar);
    return knex(USER_TABLE).insert(item);
  },
  async updateItem(id, item) {
    if (item.avatar) {
      try {
        const oldItem = await knex(USER_TABLE).select('*').where({ id }).first();
        fs.unlinkSync(oldItem.avatar);
        item.avatar = await uploadImage(item.avatar);
      } catch (err) {
        // console.error(err);
      }
    }
    const updated = await knex(USER_TABLE)
      .update({
        username: item.username || null,
        email: item.email || null,
        avatar: item.avatar || null,
        age: item.age || null,
        phone: item.phone || null,
      })
      .where({ id });

    return updated;
  },

  async patchItem(id, item) {
    if (item.avatar) {
      try {
        const oldItem = await knex(USER_TABLE).select('*').where({ id }).first();
        fs.unlinkSync(oldItem.avatar);
        item.avatar = await uploadImage(item.avatar);
      } catch (err) {
        // console.error(err);
      }
    }

    const updated = await knex(USER_TABLE).update(item).where({ id });
    return updated;
  },

  async removeItem(id) {
    const item = await knex(USER_TABLE).select('*').where({ id }).first();
    try {
      fs.unlinkSync(item.avatar);
    } catch (err) {
      // console.error(err);
    }
    return knex(USER_TABLE).where({ id }).del();
  },
};
